# wordpress test

A simple wordpress example using a mysql db
The project is using docker (docker-compose) to run a wordpress and a mysql container.
You will have to change the path where the data of the mysql db is stored. Read more in the README.

This project is based on following information:  
https://docs.docker.com/compose/wordpress/  
We used the configuration and only modified little things, so check it our to understand what we are doing.  
Our development machine is running a windows operating system, so make sure to change the path we are using if you use a different operating system.  
The path has to changed anyway, because it is unlikely that you have the same folder structure as is present on  the development machine.  
Read more in 'Before you start/Change the path' further down the page.

## How to get this

This project needs Docker to run on your Computer!
I'm not sure if it is possible to install Docker on a Windows Home Edition (You may want to install Docker first before you continue with the rest)

Open a Command Window or Powershell and check if Docker is installed:

```bash
# Check if Docker is installed
docker --version
```

You can install git and clone this repository:
* git clone [https://gitlab.com/bernhard-arida/wordpress-test.git](https://gitlab.com/bernhard-arida/wordpress-test.git)
* git clone [git@gitlab.com:bernhard-arida/wordpress-test.git](git@gitlab.com:bernhard-arida/wordpress-test.git)

You can also download this repository as a zip file and extract it.  
You can also only download the docker-compose.yml file and create the folder for the database yourself!

## Before you start

Allow docker to use the hard drive where you want to save your data:  
Go to your docker Settings (On windows right click the small docker icon in the info area)  
Go to 'Shared Drives'  
Select the shared drive and confirm.

Change the path  
```docker
volumes:db_data:driver_opts:device:  
```
It should look something like this:
```docker-compose
# Replace this with your path on your computer!
device: /d/workspaces/test-wordpress/volumes/mysql
# Replaced version example:
# Just make sure the folders exist on you computer
device: /c/my-database-save-directory/volumes/mysql
```
in the file docker-compose.yml to point to your folder where you want the data to be stored

## Steps to install

1. Download Docker and isntall it
1. [Docker for Windows](https://docs.docker.com/docker-for-windows/install/)
1. Open a command window (CMD or Git Bash)
1. Navigate to the directory
1. type in: docker-compose up

Docker should now download the mysql db docker image and the wordpress docker image.  
You can confirm this by typing in 'docker images'  
There should be a wordpress image and a mysql image now.  
If you type 'docker ps' you should see a running 'wordpress:latest' container and a running 'mysql:5.7' container.

The wordpress homepage should be available now on  
[localhost:8000](localhost:8000)  
The admin page should be available on  
[localhost:8000/wp-admin](localhost:8000/wp-admin)

## Backup your Progress

Use the wordpress export/backup to save your work!  
You can transfer the data this way to a different system.  
I recommend saving your work in a git repository.  
There are plugins available in the wordpress plugin repository.
Save your work before every single change!!!  
You can also transfer the data of the mysql database to a different mysql database. (I did not try that out so far)

## Troubleshoot

Make sure the folder structure for the db exists next to your docker-compose.yml file:  
The file structure should be like this:  
+ volumes
  + mysql
    + ...
+ docker-compose.yml
+ ...

Of course you can use a different file structure if you define a different path in the docker-compose.yml file.  
Did you check the volume path to match your system??? You should find and adjust this line in your docker-compose.yml file:   
```docker
device: /d/workspaces/test-wordpress/volumes/mysql
```

If you have problems with your docker containers not starting, try restarting it:  
First navigate to the folder where you copied or cloned the docker-compose.yml file.
We need a terminal window there (On Windows you can use 'Shift' + Right Click in the Explorer to have the option for Windows Powershell)

```bash
# Make sure docker is installed
docker --version
# Stopping of previous running containers
docker-compose down
# Check all the volumes
docker volume ls
# Removing the volume that was created (do not delete the files on the harddrive)
docker volume rm %yourvolume%
# Starting the docker containers again (volume will be recreated from the filesystem)
docker-compose up
# If it still fails, analyze the log output. It maybe lists the remove command of the volume for you.
```

## LICENSE

This project is using the MIT License, so you can do whatever you want with it.  
Have a look: [MIT License](/LICENSE)  
Make sure to check out the license conditions of the included software if you use them.
* mysql
* wordpress

## Disclaimer

This project is created for educational purposes only.  
Use this project at your own risk.  
We are not responsible for any damage that may occur if you use this project/technology, read the [MIT LICENCE](/LICENSE) for details.  
You will have to research yourself how the technologies work.  
Do not just copy the project without knowing how it works.  
Try out your backup and restore process as early as possible, so that you can save your progress.